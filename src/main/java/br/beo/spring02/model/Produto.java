package br.beo.spring02.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity //Esta classe será persistida no BD

@Table(name="produto")
public class Produto {
    
    @Id  //Este campo será uma chave primaria - Identificador
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Campo gerado pelo BD sequencialmente(1,2,3...)
    @Column(name="cod")//Nome da coluna no BD
    private int codigo;

    @Column(name="nome", length = 100, nullable = false)
    private String nome;
    
    @Column(name="valor")
    private double valor;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    

}
